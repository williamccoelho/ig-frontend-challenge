# Instituto Gourmet
## Desafio de desenvolvimento frontend

<img src="https://institutogourmet.com/app/themes/instituto-gourmet/dist/images/logo.svg" width="250">


Criamos um teste simples, porém muito efetivo para que você mostre suas habilidades 
de desenvolvimento.
Consiste em uma simples tela de Checkout [disponibilizada aqui no Figma](https://www.figma.com/file/Zjc0Ouw8gLb2vZCkdlXOqo/Checkout-Mundi.js?node-id=1%3A4) 
que você deverá reproduzir seguindo as diretrizes a abaixo:

## 1. Front-end
O front-end deverá ser desenvolvido utilizando:
- HTML
- Javascript
- Vue 2
- CSS / SASS
- E as melhores ferramentas que você necessitar para mostrar seus conhecimentos...

## 2. Ambiente de DEV
Neste repositório já temos uma imagem docker 
toda montada usando o vue cli para facilitar o processo, para inicilizar basta ter o 
docker instalado em seu computador e executar:

``
docker-compose up
``

Após isso, o ambiente de dev já instalará as depêndencias e você poderá 
começar a trabalhar em seu código com o hot reload ativo na porta :8080.


**Observação:** não é obrigatório usar essa imagem docker, sinta-se a vontade para criar um projeto 
em VUE se achar mais prático.

## 3. Obtenção de dados via API

- As informações dessa tela, serão obtidas através da API: https://app.institutogourmet.com/external-api/v1/frontend-challenge

> Você receberá uma chave de API no seu e-mail

> Autenticação via query param: https://app.institutogourmet.com/external-api/v1/frontend-challenge?api_key=<sua_chave_aqui>

> Autenticação via header Authorization: Bearer <sua_chave_aqui>



## 4. Componente de cartão de crédito

- Utilizamos no layout o componente: https://github.com/guastallaigor/vue-paycard
- Este componente tem um storybook: https://vue-paycard.netlify.app/ 
- Objetivo: implementar o formulário para preencher da melhor forma o componente
- Refinar o máximo possível para uma boa experiência final do usuário para preencher o cartão.
  

Dica: Usem o storybook do autor do componente como referência, bem como o formulário:
https://github.com/guastallaigor/vue-paycard/blob/master/tests/unit/form.vue, 
a ideia não é copiar e colar o código, mas se inspirar e criar uma própria implementação.

## 5. Envio do payload de teste - Mundi.js (opcional)

Para finalizar o desafio, uma vez preenchido os dados do cartão e o componente visual,
você deverá disparar estes dados para um ambiente de teste da Mundipagg!
A chave pública da loja de testes será enviada por email.

Documentação oficial mundi.js: https://docs.mundipagg.com/reference#checkout-transparente

### Dicas para envio do payload:

Você pode usar exatamente o mesmo formulário criado para preencher o componente visual de 
cartão de crédito.

Basta colocar o data-mundicheckout-input, para identificá-los globalmente como inputs da Mundipagg!
Abaixo uma colinha dos identificadores da documentação da própria Mundipagg:

``` html
<form action="{{url de sua action}}" method="POST" data-mundicheckout-form>
    <input type="text" name="holder-name" data-mundicheckout-input="holder_name">
    <input type="text" name="card-number" data-mundicheckout-input="number">
    <span  data-mundicheckout-input="brand"></span>
    <input type="text" name="card-exp-month" data-mundicheckout-input="exp_month">
    <input type="text" name="card-exp-year" data-mundicheckout-input="exp_year">
    <input type="text" name="cvv" data-mundicheckout-input="cvv">
    <input type="text" name="buyer-name">
    <button type="submit">Enviar</button>
</form>
```

Além de identificar os inputs, será necessário "inicializar" o script do Mundi.js, que será responsável
por obter estes dados, e tokenizá-los com a chave pública:

```js
<script src="https://checkout.mundipagg.com/transparent.js" 
        data-mundicheckout-app-id="{{chave_publica_loja_testes_email}}">
</script>
```

Você tem algumas formas de inicializar esse script de maneira que fica acertada 
com a montagem do componente e ou da SPA! Fica a seu critério a melhor forma de fazer.


## 6. Entrega

Para nos entregar seu desafio, basta responder o mesmo e-mail com o link 
para o seu repositório público .git (github, gitlab, bitbucket) contendo os arquivos 
do desafio!

Boa sorte à todos!